const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const multipart = require('connect-multiparty');
const cors = require('cors');
/*
const users_provider = require('./providers/users_provider');
const numbers_provider = require('./providers/numbers_provider');
const twilio_provider = require('./providers/twilio_provider');*/

const systems = require('./models/systems');

const app = express();

// Routes
const systems_route = require('./routes/systems');

// Port Number
const port = process.env.PORT || 8080;

// CORS Middleware
app.use(cors());

// Set Static Folder
app.use(express.static(path.join(__dirname, 'public')));

// Body Parser Middleware
app.use(bodyParser.json()); // parse json
app.use(bodyParser.urlencoded({extended: true}) ); //parse url enconded

// app.use(multipart());

// set path for the uploaded images
app.use('/uploads', express.static(path.join(__dirname, '/uploads')));


// Use Route

app.use('/systems', systems_route);
// Index Route

// set root path when build as production
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'public/index.html'));
});


// Start Server
app.listen(port, () => {
    console.log('Server started on port : ' + port);
});