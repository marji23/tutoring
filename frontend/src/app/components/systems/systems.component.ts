import { Component, OnInit } from '@angular/core';
import { SystemsService, NotificationService } from '../../services';
import { system_categories } from '../../config/system_categories';

import * as XLSX from 'xlsx';
import * as FileSaver from 'file-saver';

@Component({
    selector: 'systems-cmp',
    moduleId: module.id,
    templateUrl: 'systems.component.html',
    styleUrls: ['systems.component.css']
})

export class SystemsComponent implements OnInit{
    public numbers = [];

    model: any = {};
    smsmodel: any = {};

    system_categories = system_categories;
    filter_key: any = '';
    system_data: any = [];
    temp_system_data: any = [];
    row_editflag: any = [];
    new_system: any = {AppType: 'none', DBType: 'none'};
    checkboxes: any = [];

    constructor(private notificationService: NotificationService,
                private systemService: SystemsService) {

    }

    ngOnInit() {
        this.loadSystems();
    }

    checkEmpty(val, type) {

        let flag = 0;
        let arr = [];
        if(type == 0) { // app type
            arr = system_categories.app_type;
        } else if(type == 1) { // db type
            arr = system_categories.db_type;
        } else if(type == 2) { // sid adm
            if(val == 'nonadm') {
                return false;
            } else {
                return true;
            }
        } else if(type == 3) { // SID
            if(val == 'NON') {
                return false;
            } else {
                return true;
            }
        }

        for(let i = 0; i < arr.length; i++) {
            if(val == arr[i]) {
                flag = 1;
            }
        }
        if(val != 'none' && val != 0) {
            return true; // show val
        } else {
            return false; // hide val
        }
    }

    loadSystems() {
        console.log('filterkey : ' + this.filter_key);
        this.systemService.loadSystems({key: this.filter_key}).subscribe(data => {
            this.system_data = data.data;
            this.temp_system_data = JSON.parse(JSON.stringify(this.system_data));

            this.checkboxes.push(false);

            for(let i = 0; i < data.data.length; i++) {
                this.row_editflag.push(0); // 0: hide edit / add forms
                this.checkboxes.push(false); // false: unchecked
            }
        }, err => {
            console.log(err);
        });
    }

    cancelEdit(index) {
        this.row_editflag[index+1] = 0;
        this.system_data[index] = this.temp_system_data[index];
    }

    update(index) {
        console.log(this.system_data[index].id);
        this.systemService.updateSystems(this.system_data[index]).subscribe(data => {
            if(data.result == true) {
                this.notificationService.showNotification('updated successfully', 2);
                this.row_editflag[index+1] = 0;
            }
        }, err => {
            console.log(err);
        })
    }

    remove(index) {
        this.systemService.removeSystems({id: this.system_data[index].id}).subscribe(data => {
            if(data.result == true) {
                this.notificationService.showNotification('removed successfully', 2);
                this.loadSystems();
            }
        }, err => {
            console.log(err);
        })
    }

    addSystem() {
        this.row_editflag[0] = 1;
    }

    cancelAddEdit() {
        this.row_editflag[0] = 0;
        this.new_system = {AppType: 'none', DBType: 'none'};
    }

    save() {
        this.systemService.saveSystems(this.new_system).subscribe(data => {
            if(data.result == true) {
                this.row_editflag[0] = 0;
                this.notificationService.showNotification('added successfully', 2);
                this.loadSystems();
            }
        }, err => {
            console.log(err);
        })
    }

    allCheckboxChanged() {
        console.log(this.checkboxes[0]);
        if(this.checkboxes[0]) {
            for(let i = 0; i < this.checkboxes.length; i++) {
                this.checkboxes[i] = true;
            }
        } else {
            for(let i = 0; i < this.checkboxes.length; i++) {
                this.checkboxes[i] = false;
            }
        }
    }
    clearJSONwithNull(json) {
        let data = [];
        for(let i = 0; i < json.length; i++) {
            if(json[i] != null) {
                data.push(json[i]);
            }
        }
        return data;
    }
    exportAsExcelFile() {
        let json = [];
        if(this.checkboxes.length > 1) {
            let checkflag = 0; // nothing
            for(let i = 1; i < this.checkboxes.length; i++) {
                if(this.checkboxes[i] == true) {
                    checkflag = 1; // there is checked item
                    json.push(this.system_data[i]);
                }
            }
            if(checkflag == 1) {
                console.log('json : ' + JSON.stringify(json));
                json = this.clearJSONwithNull(json);
                const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
                const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
                const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
                this.saveAsExcelFile(excelBuffer);
            } else {
                this.notificationService.showNotification('there is no data to be exported', 3);
            }
        } else {
            this.notificationService.showNotification('there is no data to be exported', 3);
        }
    }
    saveAsExcelFile(buffer: any) {
        const data: Blob = new Blob([buffer], {
          type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8'
        });
        var today = new Date();
        var date = today.getFullYear() + '' + (today.getMonth() + 1) + '' + today.getDate() + '_';
        var time = today.getHours() + "-" + today.getMinutes() + "-" + today.getSeconds();
        var name = date + time;
        FileSaver.saveAs(data, name + '.xlsx');
    }

}
