import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

import { environment } from "../../environments/environment";

@Injectable()
export class SystemsService {
    constructor(
        private http: Http) {
    }

    loadSystems(data: any): Observable<any> {

        let headers = new Headers({ });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(environment.base_url + 'systems/load', data, options)
            .map((res: Response) => res.json())
            .catch((error: any) => {
                return Observable.throw(error);
            });
    }

    updateSystems(data: any): Observable<any> {

        let headers = new Headers({ });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(environment.base_url + 'systems/update', data, options)
            .map((res: Response) => res.json())
            .catch((error: any) => {
                return Observable.throw(error);
            });
    }

    removeSystems(data: any): Observable<any> {

        let headers = new Headers({ });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(environment.base_url + 'systems/remove', data, options)
            .map((res: Response) => res.json())
            .catch((error: any) => {
                return Observable.throw(error);
            });
    }

    saveSystems(data: any): Observable<any> {

        let headers = new Headers({ });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(environment.base_url + 'systems/save', data, options)
            .map((res: Response) => res.json())
            .catch((error: any) => {
                return Observable.throw(error);
            });
    }
}
